import { Injectable } from '@nestjs/common';
import { Responder, Requester } from 'cote';

@Injectable()
export class AppService {
  constructor() {
    const timeService = new Responder({ name: 'Time Service' });

    timeService.on('time', (req, cb) => {
      cb(new Date());
    });

    const client = new Requester({ name: 'Client' });

    client.send({ type: 'time' }, (time) => {
      console.log(time);
    });
  }
  getHello(): string {
    return 'Hello World!';
  }
}
