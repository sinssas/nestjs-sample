import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { CatsModule } from './cat/cat.module';

@Module({
  imports: [
    MongooseModule.forRoot(
      process.env.MONGOHQ_URL ||
        'mongodb://userh239r:794235bv94523rfwesr24@192.168.120.136:27017/bundle-node-nest',
    ),
    CatsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
